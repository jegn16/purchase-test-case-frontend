export function toSnake(s: string) {
  return s.replace(/[A-Z]/g, ($1) => {
    return '_' + $1.toLowerCase();
  });
}

