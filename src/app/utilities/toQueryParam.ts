export function toQueryParam(o: object): string {
  let queryFormat = '';

  Object.keys(o).forEach(key => {
    if (o[key] !== undefined) {
      if (queryFormat !== '') {
        queryFormat = `${queryFormat}&`;
      }
      queryFormat = `${queryFormat}${key}=${o[key]}`;
    }
  });

  return queryFormat;
}
