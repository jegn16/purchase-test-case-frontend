import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseTicketFormComponent } from './purchase-ticket-form.component';

describe('PurchaseTicketFormComponent', () => {
  let component: PurchaseTicketFormComponent;
  let fixture: ComponentFixture<PurchaseTicketFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseTicketFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseTicketFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
