import { Component, OnInit } from '@angular/core';
import {PurchaseTicket} from '../../entities/PurchaseTicket';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {PurchaseTicketService} from '../../services/purchase-ticket.service';
import {createNumberMask} from 'text-mask-addons';

@Component({
  selector: 'app-purchase-ticket-form',
  templateUrl: './purchase-ticket-form.component.html',
  styleUrls: ['./purchase-ticket-form.component.css']
})
export class PurchaseTicketFormComponent implements OnInit {
  purchaseTicket = new PurchaseTicket();
  currencyMask = createNumberMask({allowDecimal: true});
  loading = false;

  constructor(private purchaseTicketService: PurchaseTicketService, public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  close() {
    this.activeModal.close('Closed by user');
  }

  save() {
    this.loading = true;
    const purchaseTicket = Object.assign(new PurchaseTicket(), this.purchaseTicket);
    if (typeof purchaseTicket.totalAmount === 'string') {
      purchaseTicket.totalAmount = purchaseTicket.totalAmount.replace(/(\$|\,)/g, '');
    }
    this.purchaseTicketService.create(purchaseTicket).subscribe(createdPurchaseTicket => {
      this.loading = false;
      this.activeModal.close(createdPurchaseTicket);
    }, error => {
      this.loading = false;
    });
  }

  pickFile(event) {
    if (event.target.files.length > 0) {
      this.purchaseTicket.voucher = event.target.files[0];
    }
  }

}
