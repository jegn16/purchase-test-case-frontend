import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import {PurchaseTicketsTableComponent} from './purchase-tickets-table/purchase-tickets-table.component';
import {AuthGuard} from '../guards/auth.guard';

const routes = [
  { path: '', component: PurchaseTicketsTableComponent, canActivate: [AuthGuard] }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class HomeRoutingModule { }
