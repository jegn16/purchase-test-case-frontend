import { Component, OnInit } from '@angular/core';
import {PaginationRequest} from '../../entities/PaginationRequest';
import {PurchaseTicketService} from '../../services/purchase-ticket.service';
import {log} from 'util';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {PurchaseTicketFormComponent} from '../purchase-ticket-form/purchase-ticket-form.component';
import {Pagination} from '../../entities/Pagination';
import {PurchaseTicket} from '../../entities/PurchaseTicket';

@Component({
  selector: 'app-purchase-tickets-table',
  templateUrl: './purchase-tickets-table.component.html',
  styleUrls: ['./purchase-tickets-table.component.css']
})
export class PurchaseTicketsTableComponent implements OnInit {
  paginationRequest = new PaginationRequest();
  paginationPurchaseTickets: Pagination<PurchaseTicket>;
  sizeOptions = [5, 10, 20, 50];
  pages = [];
  voucherOwnerId: number;

  constructor(private purchaseTicketService: PurchaseTicketService, private modalService: NgbModal) { }

  ngOnInit() {
    this.paginationRequest.page = 0;
    this.paginationRequest.size = 5;
    this.loadPurchaseTickets();
  }

  openRegistrationForm() {
    const formReference = this.modalService.open(PurchaseTicketFormComponent);
    formReference.result.then(_ => {
      this.loadPurchaseTickets();
    });
  }

  loadPurchaseTickets(): void {
    const filter  = this.paginationRequest.filter;
    this.paginationRequest.filter = (filter === '') ? undefined : filter;
    this.purchaseTicketService.all(this.paginationRequest).subscribe(paginationPurchaseTickets => {
      this.paginationPurchaseTickets = paginationPurchaseTickets;
      this.generateListPages();
    }, error => {
      log('Something when wrong');
    });
  }

  loadPreviousList(): void {
    this.paginationRequest.page--;
    this.loadPurchaseTickets();
  }

  loadNextList(): void {
    this.paginationRequest.page++;
    this.loadPurchaseTickets();
  }

  generateListPages() {
    this.pages = [];
    for (let i = 1; i <= this.paginationPurchaseTickets.totalPages; i++) {
      this.pages.push(i);
    }
  }

  showVoucher(modal, voucherOwnerId: number) {
    this.voucherOwnerId = voucherOwnerId;
    this.modalService.open(modal);
  }
}
