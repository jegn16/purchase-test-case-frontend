import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImgAuthorizeComponent } from './img-authorize.component';

describe('ImgAuthorizeComponent', () => {
  let component: ImgAuthorizeComponent;
  let fixture: ComponentFixture<ImgAuthorizeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImgAuthorizeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImgAuthorizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
