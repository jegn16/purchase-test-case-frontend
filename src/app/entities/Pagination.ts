import {BaseEntity} from './BaseEntity';

export class Pagination<T> extends BaseEntity {
  totalElements: number;
  totalFilteredElements: number;
  totalPages: number;
  currentPage: number;
  elements: T[];
}
