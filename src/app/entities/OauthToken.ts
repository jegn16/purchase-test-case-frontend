export class OauthToken {
  tokenType: string;
  expiresIn: number;
  accessToken: string;
  refreshToken: string;
}
